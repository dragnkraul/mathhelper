﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathHelper.Component.Common
{
    public static class ExtensionMethods
    {
        public static bool IsSimpleDivision(this decimal value)
           {
               return (value%1) == 0;
           }
    }
}
