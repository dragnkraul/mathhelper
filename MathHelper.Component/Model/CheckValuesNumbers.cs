﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathHelper.Component.Model
{
    public class CheckValuesNumbers
    {
        public int FirstNumber { get; private set; }
        public int SecondNumber { get; private set; }

        public CheckValuesNumbers(int firstNumber, int secondNumber)
        {
            FirstNumber = firstNumber;
            SecondNumber = secondNumber;
        }
    }
}
