﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathHelper.Component.Common;
using MathHelper.Component.Model;

namespace MathHelper.Component.Managers
{
    public class CheckValues : ICheckValues
    {
        public CheckValuesNumbers SimpleDivision(int firstNumber, int secondNumber, int maxNumber, Random random)
        {
            var result = firstNumber/(decimal) secondNumber;
            while (firstNumber < secondNumber || !result.IsSimpleDivision())
            {
                firstNumber = RandomNumber(random, maxNumber);
                
                while (secondNumber == 0)
                {
                    secondNumber = RandomNumber(random, maxNumber);
                }

                result = firstNumber/(decimal) secondNumber;
            }
            return new CheckValuesNumbers(firstNumber, secondNumber);
        }
        public CheckValuesNumbers SetFirstNumberLarger(int firstNumber, int secondNumber, int maxNumber, Random random)
        {
            while (secondNumber > firstNumber)
            {
                secondNumber = RandomNumber(random, maxNumber);
            }
            return new CheckValuesNumbers(firstNumber, secondNumber);
        }
        public CheckValuesNumbers Division(int firstNumber, int secondNumber, int maxNumber, Random random)
        {
            while (secondNumber > firstNumber)
            {
                while (secondNumber != 0)
                {
                    secondNumber = RandomNumber(random, maxNumber);
                }
            }
            return new CheckValuesNumbers(firstNumber, secondNumber);
        }

        private int RandomNumber(Random random, int maxNumber = 10)
         {
             return random.Next(0, maxNumber);

         }
    }
}
