﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathHelper.Component.Model;

namespace MathHelper.Component.Managers
{
    public interface ICheckValues
    {
        CheckValuesNumbers SimpleDivision(int firstNumber, int secondNumber, int maxNumber, Random random);
        CheckValuesNumbers SetFirstNumberLarger(int firstNumber, int secondNumber, int maxNumber, Random random);
        CheckValuesNumbers Division(int firstNumber, int secondNumber, int maxNumber, Random random);
    }
}
